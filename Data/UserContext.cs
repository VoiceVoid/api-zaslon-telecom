﻿using Api_zaslon_telecom.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_zaslon_telecom.Data
{
	public class UserContext : DbContext
	{
		public UserContext(DbContextOptions<UserContext> options) :base(options)
		{
		}

		public DbSet<User> Users { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder) {
			modelBuilder.Entity<User>().ToTable("User");
  }
	}
}
