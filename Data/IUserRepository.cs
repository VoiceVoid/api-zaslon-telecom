﻿using Api_zaslon_telecom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_zaslon_telecom.Data
{
	public interface IUserRepository
	{
		Task<User> GetUser(int id);
		Task<List<User>> GetUsers(string searchTerm);
		Task<User> PostUser(User user);
	}
}
