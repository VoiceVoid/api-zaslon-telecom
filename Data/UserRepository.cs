﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api_zaslon_telecom.Models;
using Microsoft.EntityFrameworkCore;

namespace Api_zaslon_telecom.Data
{
	public class UserRepository : IUserRepository
	{
		private readonly UserContext _context;
		public UserRepository(UserContext context)
		{
			this._context = context;
		}

		public async Task<User> GetUser(int id)
		{
			return await _context.Users.Where(u => u.Id == id).FirstOrDefaultAsync();
		}

		public async Task<List<User>> GetUsers(string searchTerm)
		{
			if (searchTerm != null) 
			 {
				return await _context.Users.Where(u => u.FirstName.Contains(searchTerm) || u.LastName.Contains(searchTerm) || u.Username.Contains(searchTerm)).ToListAsync();
			 }

			return await _context.Users.ToListAsync();
			
		}

		public async Task<User> PostUser(User user)
		{
			_context.Users.Add(user);
			await _context.SaveChangesAsync();
			return user;
		}
	}
}
