﻿using Api_zaslon_telecom.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_zaslon_telecom.Data
{
	public class Seed
	{
		private readonly UserContext _context;
		public Seed(UserContext context)
		{
			this._context = context;
		}

		public void SeedUsers()
		{
			if (!_context.Users.Any())
			{
				var userData = System.IO.File.ReadAllText("Data/MOCK_DATA.json");
				var users = JsonConvert.DeserializeObject<List<User>>(userData);
				foreach (var user in users)
				{
					_context.Users.Add(user);
				}
				_context.SaveChanges();
			}
		}
	}
}
