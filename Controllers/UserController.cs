﻿using Api_zaslon_telecom.Data;
using Api_zaslon_telecom.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_zaslon_telecom.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private readonly IUserRepository _repo;

		public UserController(IUserRepository repo)
		{
			_repo = repo;
		}

		//Get all users
		public async Task<ActionResult<List<User>>> GetUsers(string? searchTerm)
		{
			var users = await _repo.GetUsers(searchTerm);
			return Ok(users);
		}

		// Get by Id
		[HttpGet("{id}")]
		public async Task<ActionResult<User>> GetUser(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}
			User user = await _repo.GetUser((int)id);

			if (user == null)
			{
				return NotFound();
			}

			return Ok(user);
		}
		// create user
		[HttpPost]
		public async Task<ActionResult<User>> PostUser(User user)
		{
			await _repo.PostUser(user);
			return CreatedAtAction("GetUser", new { id = user.Id }, user);
		}
	}
}
